#!/bin/bash
# Install Rust-based upgrades to common utilities

if ! command -v cargo &>/dev/null; then
	echo "cargo not found"
	exit
fi

# zoxide -> cd
# cargo, brew

# bottom -> top
# cargo, brew

# dust -> du
# cargo, brew

# rip -> rm
# cargo, brew (rm-improved)

# lsd -> ls
# cargo, brew

# bat -> cat/less
# cargo, brew

# fd -> find
# cargo, brew

# ripgrep -> grep
# cargo, brew

# runiq -> uniq
# cargo

# mprocs -> parallel
# cargo, brew

# pueue
# cargo, brew
