#!/bin/bash
# Install Rust toolchain

if [[ -d "$CARGO_HOME" ]]; then
	echo "Rust is already installed."
	exit
fi

if [[ -d "$HOME/.cargo" ]]; then
	echo "Rust is already installed."
	exit
fi

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
