#!/bin/sh
# Install/update kitty

LOCALVERSION=$(kitty -v | sed 's/^kitty \([0-9.]*\).*/\1/')
CURRENTVERSION=$(curl -fsSL "https://sw.kovidgoyal.net/kitty/current-version.txt")

if [ "$LOCALVERSION" = "$CURRENTVERSION" ]; then
	echo "kitty is up to date"
	exit
fi

curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
